package de.comhix.commons.logging.versionTest;

/**
 * @author Benjamin Beeker
 */
public class VersionStaticProperty {
    public static final String VERSION = "1";
}
