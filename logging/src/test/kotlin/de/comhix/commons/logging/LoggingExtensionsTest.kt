package de.comhix.commons.logging

import org.amshove.kluent.`should be equal to`
import org.testng.annotations.Test
import java.util.concurrent.CountDownLatch
import java.util.logging.Level

/**
 * @author Benjamin Beeker
 */
class LoggingExtensionsTest {
    private class TestLoggerHolder

    private val testLoggerHolder = TestLoggerHolder()

    @Test
    fun `logging uses messageProvider`() {
        info { "logging uses messageProvider" }

        // given
        val latch = CountDownLatch(1)
        testLoggerHolder.logger().level = Level.FINE

        // when
        testLoggerHolder.debug {
            latch.countDown()
            "foo"
        }

        // then
        latch.count `should be equal to` 0
    }

    @Test
    fun `logging skips messageProvider`() {
        info { "logging skips messageProvider" }

        // given
        val latch = CountDownLatch(1)
        testLoggerHolder.logger().level = Level.INFO

        // when
        testLoggerHolder.debug {
            latch.countDown()
            "foo"
        }

        // then
        latch.count `should be equal to` 1
    }
}