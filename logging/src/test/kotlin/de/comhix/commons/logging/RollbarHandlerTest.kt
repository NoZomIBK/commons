package de.comhix.commons.logging

import de.comhix.commons.logging.versionTest.*
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be null`
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
class RollbarHandlerTest {
    @DataProvider(name = "version class provider")
    fun `version class provider`(): Iterator<Array<Any>> {
        val testData = mutableListOf<Array<Any>>()

        testData.add(arrayOf("${VersionObjectProperty::class.qualifiedName}::VERSION"))
        testData.add(arrayOf("${VersionObjectFunction::class.qualifiedName}::VERSION"))
        testData.add(arrayOf("${VersionClassProperty::class.qualifiedName}::VERSION"))
        testData.add(arrayOf("${VersionClassFunction::class.qualifiedName}::VERSION"))
        testData.add(arrayOf("${VersionCompanionObjectProperty::class.qualifiedName}::VERSION"))
        testData.add(arrayOf("${VersionCompanionObjectFunction::class.qualifiedName}::VERSION"))
        testData.add(arrayOf("${VersionStaticProperty::class.qualifiedName}::VERSION"))
        testData.add(arrayOf("${VersionStaticFunction::class.qualifiedName}::VERSION"))

        return testData.iterator()
    }

    @Test(dataProvider = "version class provider")
    fun getVersion(property: String) {
        info { "getVersion($property)" }

        // when
        val version = RollbarHandler.getVersion<Any>(property)

        // then
        version `should be equal to` "1"
    }

    @Test
    fun `getVersion class without default constructor`() {
        info { "getVersion class without default constructor" }

        // when
        val version = RollbarHandler.getVersion<Any>(VersionCompanionObjectFunction::class.qualifiedName)

        // then
        version.`should be null`()
    }

    @Test
    fun `getVersion class not found`() {
        info { "getVersion class not found" }

        // when
        val version = RollbarHandler.getVersion<Any>("foobar")

        // then
        version.`should be null`()
    }
}