@file:Suppress("TestFunctionName", "PropertyName")

package de.comhix.commons.logging.versionTest

/**
 * @author Benjamin Beeker
 */
object VersionObjectProperty {
    const val VERSION = "1"
}

object VersionObjectFunction {
    fun VERSION() = "1"
}

class VersionClassProperty {
    val VERSION = "1"
}

class VersionClassFunction {
    fun VERSION() = "1"
}

class VersionCompanionObjectProperty {
    companion object {
        const val VERSION = "1"
    }
}

class VersionCompanionObjectFunction {
    companion object {
        fun VERSION() = "1"
    }
}

class NoDefaultConstructor(private val foobar: String)