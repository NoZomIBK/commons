package de.comhix.commons.logging

import java.io.OutputStream
import java.util.logging.ConsoleHandler

class StdHandler : ConsoleHandler() {
    override fun setOutputStream(out: OutputStream?) {
        super.setOutputStream(System.out)
    }
}