package de.comhix.commons.logging

import com.rollbar.api.payload.data.Level
import com.rollbar.api.payload.data.Level.*
import com.rollbar.notifier.Rollbar
import com.rollbar.notifier.config.ConfigBuilder
import org.slf4j.MDC
import java.util.logging.Formatter
import java.util.logging.Handler
import java.util.logging.LogManager
import java.util.logging.LogRecord
import kotlin.collections.HashMap
import kotlin.reflect.KClass
import kotlin.reflect.full.*

/**
 * @author Benjamin Beeker
 */
class RollbarHandler : Handler() {
    companion object {
        private val PREFIX = RollbarHandler::class.qualifiedName
        private const val DELIMITER = "::"

        @Suppress("UNCHECKED_CAST")
        internal fun <Type : Any> getVersion(provider: String? = readProperty("versionProvider")): String? {
            provider ?: return null

            val (clazz, member) = if (provider.contains(DELIMITER)) {
                Pair(provider.split(DELIMITER, limit = 2)[0], provider.split(DELIMITER, limit = 2)[1])
            }
            else {
                Pair(provider, "BUILD_VERSION")
            }

            try {
                val providerClass: KClass<Type> = Class.forName(clazz).kotlin as KClass<Type>

                val version = providerClass.objectInstance?.let { instance ->
                    providerClass.memberProperties.firstOrNull { it.name == member }?.getter?.call()
                    ?: providerClass.memberFunctions.firstOrNull { it.name == member }?.call(instance)
                }
                              ?: providerClass.staticProperties.firstOrNull { it.name == member }?.invoke()
                              ?: providerClass.staticFunctions.firstOrNull { it.name == member }?.call()
                              ?: providerClass.companionObjectInstance?.let { instance ->
                                  (providerClass.companionObject as KClass<Any>?)?.memberProperties?.firstOrNull { it.name == member }
                                          ?.get(instance)
                                  ?: providerClass.companionObject?.memberFunctions?.firstOrNull { it.name == member }
                                          ?.call(instance)
                              }
                              ?: let {
                                  val providerInstance: Type = providerClass.createInstance()
                                  providerClass.memberProperties.firstOrNull { it.name == member }?.get(providerInstance)
                                  ?: providerClass.memberFunctions.firstOrNull { it.name == member }?.call(providerInstance)
                              }



                if (version is String) {
                    return version
                }
            }
            catch (_: Exception) {
            }
            return null
        }

        private fun readProperty(property: String): String? = getProperty("$PREFIX.$property")

        private fun getProperty(key: String): String? {
            return System.getProperty(key)?:LogManager.getLogManager().getProperty(key)
        }
    }

    private val rollbar: Rollbar by lazy {
        configure()
        val config = ConfigBuilder.withAccessToken(readProperty("apiKey"))
                .environment(readProperty("environment"))
                .platform(readProperty("platform"))
                .codeVersion(getVersion<Any>())
                .build()
        Rollbar(config)
    }

    private fun configure() {
        formatter = object : Formatter() {
            override fun format(record: LogRecord): String {
                return formatMessage(record)
            }
        }
    }

    override fun publish(record: LogRecord) {
        if (!isLoggable(record)) {
            return
        }
        val level: Level = when {
            record.level === java.util.logging.Level.SEVERE  -> ERROR
            record.level === java.util.logging.Level.WARNING -> WARNING
            record.level === java.util.logging.Level.INFO    -> INFO
            else                                             -> DEBUG
        }
        val customData: MutableMap<String, Any> = HashMap()
        customData["source"] = record.sourceClassName + "::" + record.sourceMethodName
        val mdcMap = MDC.getCopyOfContextMap()
        if (mdcMap != null) {
            customData.putAll(mdcMap)
        }
        rollbar.log(record.thrown, customData, formatter.formatMessage(record), level)
    }

    override fun flush() {}

    @Throws(SecurityException::class)
    override fun close() {
        try {
            rollbar.close(true)
        }
        catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}