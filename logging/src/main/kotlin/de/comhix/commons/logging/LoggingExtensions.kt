// Inline needed so the "this::class" in "logger()" works
@file:Suppress("NOTHING_TO_INLINE", "unused")

package de.comhix.commons.logging

import java.util.logging.Level
import java.util.logging.Level.*
import java.util.logging.Logger
import kotlin.reflect.KClass

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("info{message}"))
inline fun Any.info(message: String) = logger().log(INFO, message)
inline fun Any.info(noinline supplier: () -> String) = logger().log(INFO, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("info(throwable){message}"))
inline fun Any.info(message: String, throwable: Throwable) = logger().log(INFO, message, throwable)
inline fun Any.info(noinline supplier: () -> String, throwable: Throwable) = logger().log(INFO, throwable, supplier)
inline fun Any.info(throwable: Throwable, noinline supplier: () -> String) = logger().log(INFO, throwable, supplier)

inline fun Any.info(message: String, param: Any) = logger().log(INFO, message, param)
inline fun Any.info(message: String, vararg params: Any) = logger().log(INFO, message, params)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("debug{message}"))
inline fun Any.debug(message: String) = logger().log(FINE, message)
inline fun Any.debug(noinline supplier: () -> String) = logger().log(FINE, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("debug(throwable){message}"))
inline fun Any.debug(message: String, throwable: Throwable) = logger().log(FINE, message, throwable)
inline fun Any.debug(noinline supplier: () -> String, throwable: Throwable) = logger().log(FINE, throwable, supplier)
inline fun Any.debug(throwable: Throwable, noinline supplier: () -> String) = logger().log(FINE, throwable, supplier)

inline fun Any.debug(message: String, param: Any) = logger().log(FINE, message, param)
inline fun Any.debug(message: String, vararg params: Any) = logger().log(FINE, message, params)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("warn{message}"))
inline fun Any.warn(message: String) = logger().log(WARNING, message)
inline fun Any.warn(noinline supplier: () -> String) = logger().log(WARNING, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("warn(throwable){message}"))
inline fun Any.warn(message: String, throwable: Throwable) = logger().log(WARNING, message, throwable)
inline fun Any.warn(noinline supplier: () -> String, throwable: Throwable) = logger().log(WARNING, throwable, supplier)
inline fun Any.warn(throwable: Throwable, noinline supplier: () -> String) = logger().log(WARNING, throwable, supplier)

inline fun Any.warn(message: String, param: Any) = logger().log(WARNING, message, param)
inline fun Any.warn(message: String, vararg params: Any) = logger().log(WARNING, message, params)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("error{message}"))
inline fun Any.error(message: String) = logger().log(SEVERE, message)
inline fun Any.error(noinline supplier: () -> String) = logger().log(SEVERE, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("error(throwable){message}"))
inline fun Any.error(message: String, throwable: Throwable) = logger().log(SEVERE, message, throwable)
inline fun Any.error(noinline supplier: () -> String, throwable: Throwable) = logger().log(SEVERE, throwable, supplier)
inline fun Any.error(throwable: Throwable, noinline supplier: () -> String) = logger().log(SEVERE, throwable, supplier)

inline fun Any.error(message: String, param: Any) = logger().log(SEVERE, message, param)
inline fun Any.error(message: String, vararg params: Any) = logger().log(SEVERE, message, params)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("log(level){message}"))
inline fun Any.log(level: Level, message: String) = logger().log(level, message)
inline fun Any.log(level: Level, noinline supplier: () -> String) = logger().log(level, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("log(level,throwable){message}"))
inline fun Any.log(level: Level, message: String, throwable: Throwable) = logger().log(level, message, throwable)
inline fun Any.log(level: Level, noinline supplier: () -> String, throwable: Throwable) = logger().log(level, throwable, supplier)
inline fun Any.log(level: Level, throwable: Throwable, noinline supplier: () -> String) = logger().log(level, throwable, supplier)

inline fun Any.log(level: Level, message: String, param: Any) = logger().log(level, message, param)

inline fun Any.log(level: Level, message: String, vararg params: Any) = logger().log(level, message, params)

fun Any.logger() = logger(this::class)

fun logger(klass: KClass<out Any>) = Logger.getLogger(klass.qualifiedName)!!