dependencies {
    api("com.rollbar:rollbar-java:1.7.3")
    implementation(kotlin("reflect"))
}

publishing {
    publications {
        getByName<MavenPublication>("default") {
            pom {
                name.set("ComHix logging")
                description.set("Package with all needed dependencies and ready to go config for logging.")
            }
        }
    }
}