# Usage: 

``` bash
git remote add template-origin git@gitlab.com:NoZomIBK/gradle-template.git  
git remote set-url template-origin --push "push not allowed"  
git pull template-origin library
```

Maybe first time needed:
``` bash
git pull template-origin library --allow-unrelated-histories
```

# Possible Branches:
- master
- library
- standalone
- standalone-locked
- docker