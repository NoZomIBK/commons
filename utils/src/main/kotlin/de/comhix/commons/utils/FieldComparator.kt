package de.comhix.commons.utils

import kotlin.math.abs
import kotlin.math.pow
import kotlin.reflect.KProperty1

/**
 * @author Benjamin Beeker
 */
class FieldComparatorBuilder<Type : Any> {
    private val fields: MutableList<FieldEntry<Type, *>> = mutableListOf()

    fun <ValueType : Comparable<ValueType>?> field(field: KProperty1<Type, ValueType?>, nullFirst: Boolean = false): FieldComparatorBuilder<Type> {
        field(field) { o1, o2 ->
            val nullSorter = if (nullFirst) -1 else 1
            if (o1 != null && o2 != null) {
                o1.compareTo(o2)
            }
            else if (o1 == null && o2 == null) {
                0
            }
            else if (o1 == null) {
                nullSorter
            }
            else {
                nullSorter * -1
            }
        }
        return this
    }

    fun <ValueType> field(field: KProperty1<Type, ValueType>, comparator: Comparator<ValueType>): FieldComparatorBuilder<Type> {
        fields.add(FieldEntry(field, comparator))
        return this
    }

    fun build() = FieldComparator(fields)
}

class FieldComparator<Type : Any> internal constructor(private val fields: List<FieldEntry<Type, *>>) : Comparator<Type> {
    override fun compare(o1: Type, o2: Type): Int {
        return fields.mapIndexed { index, fieldEntry ->
            val factor = (fields.size - index).toDouble().pow(2.0)
            val comparison = fieldEntry.compare(o1, o2)
            val standardizedComparison = if (comparison == 0) comparison else comparison / abs(comparison)
            (standardizedComparison * factor).toInt()
        }.sum()
    }
}

internal class FieldEntry<Klass, ValueType> constructor(val field: KProperty1<Klass, ValueType>, private val comparator: Comparator<ValueType>) {
    fun compare(entity1: Klass, entity2: Klass) = comparator.compare(field.get(entity1), field.get(entity2))
}