package de.comhix.commons.utils

import de.comhix.commons.logging.info
import org.amshove.kluent.`should be equal to`
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

/**
 * @author Benjamin Beeker
 */
class FieldComparatorBuilderTest {

    @DataProvider(name = "comparable objects")
    fun `comparable objects`(): Iterator<Array<Any>> {
        val testData = mutableListOf<Array<Any>>()

        testData.add(arrayOf(listOf(TestDataClass(1, "a", listOf()), TestDataClass(2, "a", listOf()))))
        testData.add(arrayOf(listOf(TestDataClass(1, "a", listOf()), TestDataClass(1, "b", listOf()))))
        testData.add(arrayOf(listOf(TestDataClass(1, "a", listOf()), TestDataClass(1, "a", listOf("")))))
        testData.add(arrayOf(listOf(TestDataClass(1, "a", listOf("", "", "", "", "", "", "")), TestDataClass(2, "a", listOf()))))

        return testData.iterator()
    }

    @Test(dataProvider = "comparable objects")
    fun `compare by fields`(objectList: List<TestDataClass>) {
        info { "compare by fields" }

        // given
        val comparator: FieldComparator<TestDataClass> = FieldComparatorBuilder<TestDataClass>()
                .field(TestDataClass::x1)
                .field(TestDataClass::x2)
                .field(TestDataClass::x3) { o1, o2 -> o1.size - o2.size }
                .field(TestDataClass::x4)
                .build()

        val reversedList = objectList.reversed()

        // when
        val sorted = reversedList.sortedWith(comparator)

        // then
        sorted `should be equal to` objectList
    }

    @Test
    fun `null first`() {
        info { "null first" }

        // given
        val objectList = listOf(TestDataClass(1, "a", listOf()), TestDataClass(1, "a", listOf(), "a"))
        val comparator: FieldComparator<TestDataClass> = FieldComparatorBuilder<TestDataClass>()
                .field(TestDataClass::x1)
                .field(TestDataClass::x4, true)
                .build()

        val reversedList = objectList.reversed()

        // when
        val sorted = reversedList.sortedWith(comparator)

        // then
        sorted `should be equal to` objectList
    }

    @Test
    fun `null last`() {
        info { "null last" }

        // given
        val objectList = listOf(TestDataClass(1, "a", listOf(), "a"), TestDataClass(1, "a", listOf()))
        val comparator: FieldComparator<TestDataClass> = FieldComparatorBuilder<TestDataClass>()
                .field(TestDataClass::x1)
                .field(TestDataClass::x4, false)
                .build()

        val reversedList = objectList.reversed()

        // when
        val sorted = reversedList.sortedWith(comparator)

        // then
        sorted `should be equal to` objectList
    }

    @Test
    fun `nullable comparator`() {
        info { "nullable comparator" }

        // given
        val objectList = listOf(TestDataClass(1, "a", listOf()), TestDataClass(1, "a", listOf(), "a"))
        val comparator: FieldComparator<TestDataClass> = FieldComparatorBuilder<TestDataClass>()
                .field(TestDataClass::x1)
                .field(TestDataClass::x4) { o1, o2 -> (o1?.length ?: 0) - (o2?.length ?: 0) }
                .build()

        val reversedList = objectList.reversed()

        // when
        val sorted = reversedList.sortedWith(comparator)

        // then
        sorted `should be equal to` objectList
    }
}

data class TestDataClass(val x1: Int, val x2: String, val x3: List<String>, val x4: String? = null)