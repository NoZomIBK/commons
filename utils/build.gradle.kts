publishing {
    publications {
        getByName<MavenPublication>("default") {
            pom {
                name.set("ComHix utils")
                description.set("yet another utils package")
            }
        }
    }
}