//package de.comhix.commons.updater
//
//import com.google.gson.Gson
//import org.assertj.core.api.Assertions
//import org.mockito.Mockito
//import org.testng.annotations.Test
//import java.io.ByteArrayInputStream
//import java.util.logging.Logger
//
///**
// * @author Benjamin Beeker
// */
//class UpdaterTest {
//    @Test
//    @Throws(Exception::class)
//    fun testNewVersionAvailable() {
//        LOG.info("testNewVersionAvailable")
//
//        // given
//        val currentVersion = UpdateInfo()
//        currentVersion.version = 3
//        var updater = Updater.create("bla", currentVersion, { obj: UpdateInfo -> obj.url!! }, { obj: UpdateInfo -> obj.version })
//        val newVersion = UpdateInfo()
//        newVersion.version = 4
//        val response = Gson().toJson(newVersion)
//        updater = Mockito.spy(updater)
//        Mockito.doReturn(ByteArrayInputStream(response.toByteArray())).`when`(updater).loadInfo()
//
//        // when
//        val hasUpdate = updater.hasUpdate()
//
//        // then
//        Assertions.assertThat(hasUpdate).isTrue
//    }
//
//    @Test
//    @Throws(Exception::class)
//    fun testSameVersionAvailable() {
//        LOG.info("testSameVersionAvailable")
//
//        // given
//        val currentVersion = UpdateInfo()
//        currentVersion.version = 3
//        var updater = Updater.create("bla", currentVersion, { obj: UpdateInfo -> obj.url!! }, { obj: UpdateInfo -> obj.version })
//        val newVersion = UpdateInfo()
//        newVersion.version = 3
//        val response = Gson().toJson(newVersion)
//        updater = Mockito.spy(updater)
//        Mockito.doReturn(ByteArrayInputStream(response.toByteArray())).`when`(updater).loadInfo()
//
//        // when
//        val hasUpdate = updater.hasUpdate()
//
//        // then
//        Assertions.assertThat(hasUpdate).isFalse
//    }
//
//    private class UpdateInfo {
//        var url: String? = null
//        var version = 0
//    }
//
//    companion object {
//        private val LOG = Logger.getLogger(UpdaterTest::class.java.name)
//    }
//}