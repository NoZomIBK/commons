package de.comhix.commons.updater

import java.awt.GraphicsEnvironment
import java.io.IOException
import java.util.logging.Logger
import javax.swing.JOptionPane
import kotlin.system.exitProcess

/**
 * @author Benjamin Beeker
 */
object UpdaterInterface {
    private val log = Logger.getLogger(UpdaterInterface::class.java.name)

    @Throws(IOException::class)
    fun <Info : Any, Version : Comparable<Version>> tryUpdate(url: String,
                                                              currentVersion: Info,
                                                              updateUrlProvider: (Info) -> String,
                                                              versionProvider: (Info) -> Version,
                                                              autoUpdate: Boolean): Boolean {
        val updater: Updater<Info, Version> = Updater.create(url, currentVersion, updateUrlProvider, versionProvider)
        if (updater.hasUpdate()) {
            var doUpdate = autoUpdate
            if (!autoUpdate) {
                if (!GraphicsEnvironment.isHeadless()) {
                    when (JOptionPane.showConfirmDialog(null, "Update available. Load it now?", "Update", JOptionPane.YES_NO_OPTION)) {
                        JOptionPane.YES_OPTION -> doUpdate = true
                        JOptionPane.NO_OPTION -> log.fine("aborting update")
                        else                   -> {
                            log.fine("update dialog closed, stopping application")
                            exitProcess(0)
                        }
                    }
                }
                else {
                    log.info("Update found. Use update-mode if available or execute in graphic-mode.")
                }
            }
            if (doUpdate) {
                updater.doUpdate()
                return true
            }
        }
        return false
    }
}