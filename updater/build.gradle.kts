dependencies {
    implementation("com.squareup.okhttp3:okhttp:3.11.0")
    implementation("com.google.code.gson:gson:2.8.0")
    testImplementation("org.assertj:assertj-core-java8:1.0.0m1")
    testImplementation("org.mockito:mockito-all:2.0.2-beta")
}
publishing {
    publications {
        getByName<MavenPublication>("default") {
            pom {
                name.set("ComHix updater")
                description.set("Base for updating applications")
            }
        }
    }
}