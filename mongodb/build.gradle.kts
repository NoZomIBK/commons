dependencies {
    implementation(kotlin("reflect"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("org.reflections:reflections:0.9.12")
    implementation("com.google.guava:guava:29.0-jre")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation("org.apache.commons:commons-lang3:3.4")
    implementation("com.google.inject:guice:4.1.0")
    api("org.mongodb.morphia:morphia:1.3.1")
    api("com.fasterxml.jackson.module:jackson-module-kotlin:2.11.3")
    testImplementation("com.github.fakemongo:fongo:2.1.0")
}

publishing {
    publications {
        getByName<MavenPublication>("default") {
            pom {
                name.set("ComHix mongodb")
                description.set("Base classes for working with Mongodb")
            }
        }
    }
}