package de.comhix.database.utility

import de.comhix.database.objects.DatabaseObject
import org.mongodb.morphia.Datastore

/**
 * @author Benjamin Beeker
 */
class IndexCreator(private val datastore: Datastore) {

    fun ensureIndexes() {
        val databaseClasses = DatabaseObject::class.getSubTypes<DatabaseObject>().filter { !it.isAbstract }
        databaseClasses.forEach { kClass ->
            datastore.ensureIndexes(kClass.java)
        }
    }
}