package de.comhix.database.utility

import com.fasterxml.jackson.annotation.JsonProperty
import de.comhix.database.objects.DatabaseObject
import org.reflections.Reflections
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.findAnnotation

/**
 * @author Benjamin Beeker
 */

val reflections = Reflections("de.comhix")

inline fun <reified SuperType : Any> KClass<*>.getSubTypes(): List<KClass<out SuperType>> =
        reflections.getSubTypesOf(this.java).map { it.kotlin as KClass<out SuperType> }