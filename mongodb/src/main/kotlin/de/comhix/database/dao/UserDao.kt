package de.comhix.database.dao

import com.google.inject.Inject
import de.comhix.database.dao.Query.Operation
import de.comhix.database.objects.HasUserId
import org.bson.types.ObjectId
import javax.inject.Named

/**
 * @author Benjamin Beeker
 */
open class UserDao @Inject constructor(private val baseDao: BaseDao, @Named(USER_INJECT) private val user: String)
    : SimpleDao<HasUserId> {
    override suspend fun <Type : HasUserId> get(id: String, typeClass: Class<Type>): Type? {
        return baseDao.get(id, typeClass)?.let {
            if (it.userId == user) {
                it
            }
            else {
                null
            }
        }
    }

    override suspend fun <Type : HasUserId> save(instance: Type): Type {
        return baseDao.get(instance.id, instance.javaClass).let {
            if (it != null && it.userId != user) {
                instance.id = ObjectId().toString()
                instance.version = 0
            }
            instance.userId = user
            baseDao.save(instance)
        }
    }

    override suspend fun <Type : HasUserId> delete(id: String, typeClass: Class<Type>) {
        baseDao.get(id, typeClass)?.let{
            if (it.userId == user) {
                baseDao.delete(id,typeClass)
            }
        }
    }

    override suspend fun <Type : HasUserId> query(typeClass: Class<Type>): Query<Type> {
        return baseDao.query(typeClass).with("user", Operation.EQ, user)
    }

    companion object {
        const val USER_INJECT = "User_Inject"
    }
}