package de.comhix.database.dao

import de.comhix.database.objects.DatabaseObject
import kotlinx.coroutines.runBlocking

/**
 * @author Benjamin Beeker
 */
open class SyncDaoWrapper<ObjectBase : DatabaseObject>(private val dao: SimpleDao<ObjectBase>) {
    fun <Type : ObjectBase> get(id: String, typeClass: Class<Type>) = runBlocking { dao.get(id, typeClass) }
    fun <Type : ObjectBase> save(instance: Type) = runBlocking { dao.save(instance) }
    fun <Type : ObjectBase> delete(id: String, typeClass: Class<Type>) = runBlocking { dao.delete(id, typeClass) }
    fun <Type : ObjectBase> query(typeClass: Class<Type>) = runBlocking { dao.query(typeClass) }
}