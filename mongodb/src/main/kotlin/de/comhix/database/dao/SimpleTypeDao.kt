package de.comhix.database.dao

import de.comhix.database.objects.DatabaseObject

/**
 * @author Benjamin Beeker
 */
interface SimpleTypeDao<ObjectBase : DatabaseObject> {
    suspend fun get(id: String): ObjectBase?
    suspend fun save(instance: ObjectBase): ObjectBase
    suspend fun delete(id: String)
    suspend fun query(): Query<ObjectBase>
}