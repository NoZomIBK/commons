package de.comhix.database.dao

import de.comhix.database.objects.DatabaseObject
import kotlinx.coroutines.runBlocking

/**
 * @author Benjamin Beeker
 */
open class SyncTypeDaoWrapper<ObjectBase : DatabaseObject>(private val dao: SimpleTypeDao<ObjectBase>) {
    fun get(id: String) = runBlocking { dao.get(id) }
    fun save(`object`: ObjectBase) = runBlocking { dao.save(`object`) }
    fun delete(id: String) = runBlocking { dao.delete(id) }
    fun query() = runBlocking { dao.query() }
}