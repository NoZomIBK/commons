package de.comhix.database.dao

import de.comhix.database.objects.QueryResult
import org.mongodb.morphia.Datastore
import org.mongodb.morphia.query.FindOptions
import kotlin.reflect.KProperty

/**
 * @author Benjamin Beeker
 */
open class Query<Type> {
    private lateinit var query: org.mongodb.morphia.query.Query<Type>
    private val options = FindOptions()

    /**
     * only for [de.comhix.database.dao.test.MockQuery]
     */
    protected constructor()

    constructor(typeClass: Class<Type>, datastore: Datastore) {
        query = datastore.createQuery(typeClass)
    }

    open fun <AllowedValueType> with(field: KProperty<Any?>, operation: Operation<AllowedValueType>, value: AllowedValueType) =
            with(field.name, operation, value)

    open fun <AllowedValueType> with(field: String, operation: Operation<AllowedValueType>, value: AllowedValueType): Query<Type> {
        when {
            operation === Operation.EQ       -> query.field(field).equal(value)
            operation === Operation.NEQ      -> query.field(field).notEqual(value)
            operation === Operation.GT       -> query.field(field).greaterThan(value)
            operation === Operation.GEQ      -> query.field(field).greaterThanOrEq(value)
            operation === Operation.LT       -> query.field(field).lessThan(value)
            operation === Operation.LEQ      -> query.field(field).lessThanOrEq(value)
            operation === Operation.EXISTS   -> if (value as Boolean) query.field(field).exists() else query.field(field).doesNotExist()
            operation === Operation.IN       -> query.field(field).`in`(value as Iterable<*>)
            operation === Operation.NOT_IN   -> query.field(field).notIn(value as Iterable<*>)
            operation === Operation.HAS      -> query.field(field).hasThisOne(value)
            operation === Operation.HAS_ANY  -> query.field(field).hasAnyOf(value as Iterable<*>)
            operation === Operation.HAS_NONE -> query.field(field).hasNoneOf(value as Iterable<*>)
            operation === Operation.HAS_ALL  -> query.field(field).hasAllOf(value as Iterable<*>)
        }
        return this
    }

    open fun limit(limit: Int): Query<Type> {
        options.limit(limit)
        return this
    }

    open fun skip(skip: Int): Query<Type> {
        options.skip(skip)
        return this
    }

    open fun order(field: KProperty<Any>): Query<Type> = order(field.name)

    open fun order(field: String): Query<Type> {
        query.order(field)
        return this
    }

    open suspend fun query(): QueryResult<Type> {
        return QueryResult(query.asList(), query.count())
    }

    open suspend fun find(): Type? {
        limit(1)
        return query().let {
            if (!it.isEmpty()) {
                it[0]
            }
            else {
                null
            }
        }
    }

    class Operation<AllowedValueType> {
        companion object {
            val EQ: Operation<Any> = Operation()
            val NEQ: Operation<Any> = Operation()
            val GT: Operation<Comparable<*>> = Operation()
            val GEQ: Operation<Comparable<*>> = Operation()
            val LT: Operation<Comparable<*>> = Operation()
            val LEQ: Operation<Comparable<*>> = Operation()
            val EXISTS: Operation<Boolean> = Operation()
            val IN: Operation<Iterable<*>> = Operation()
            val NOT_IN: Operation<Iterable<*>> = Operation()
            val HAS: Operation<Any> = Operation()
            val HAS_ANY: Operation<Iterable<*>> = Operation()
            val HAS_NONE: Operation<Iterable<*>> = Operation()
            val HAS_ALL: Operation<Iterable<*>> = Operation()
        }
    }
}