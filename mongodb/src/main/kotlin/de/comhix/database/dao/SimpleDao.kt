package de.comhix.database.dao

import de.comhix.database.objects.DatabaseObject
import kotlin.reflect.KClass

/**
 * @author Benjamin Beeker
 */
interface SimpleDao<ObjectBase : DatabaseObject> {
    suspend fun <Type : ObjectBase> get(id: String, typeClass: Class<Type>): Type?
    suspend fun <Type : ObjectBase> get(id: String, typeClass: KClass<Type>) = get(id, typeClass.java)
    suspend fun <Type : ObjectBase> save(instance: Type): Type
    suspend fun <Type : ObjectBase> delete(id: String, typeClass: Class<Type>)
    suspend fun <Type : ObjectBase> delete(id: String, typeClass: KClass<Type>) = delete(id, typeClass.java)
    suspend fun <Type : ObjectBase> query(typeClass: Class<Type>): Query<Type>
    suspend fun <Type : ObjectBase> query(typeClass: KClass<Type>) = query(typeClass.java)
}