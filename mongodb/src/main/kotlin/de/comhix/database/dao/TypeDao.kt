package de.comhix.database.dao

import de.comhix.database.objects.DatabaseObject
import javax.inject.Inject
import javax.inject.Named
import kotlin.reflect.KClass

/**
 * @author Benjamin Beeker
 */
open class TypeDao<Type : DatabaseObject> @Inject constructor(private val baseDao: BaseDao,
                                                              @Named(NAMED_TYPE_INJECT_PARAM) private val typeClass: Class<Type>)
    : SimpleTypeDao<Type> {

    constructor(baseDao: BaseDao, typeClass: KClass<Type>) : this(baseDao, typeClass.java)

    override suspend fun get(id: String): Type? {
        return baseDao.get(id, typeClass)
    }

    override suspend fun save(instance: Type): Type {
        return baseDao.save(instance)
    }

    override suspend fun delete(id: String) {
        return baseDao.delete(id, typeClass)
    }

    override suspend fun query(): Query<Type> {
        return baseDao.query(typeClass)
    }

    companion object {
        const val NAMED_TYPE_INJECT_PARAM = "NAMED_TYPE_INJECT_PARAM"
    }
}

@Suppress("FunctionName")
inline fun <reified Type : DatabaseObject> TypeDao(baseDao: BaseDao) = TypeDao<Type>(baseDao, Type::class)