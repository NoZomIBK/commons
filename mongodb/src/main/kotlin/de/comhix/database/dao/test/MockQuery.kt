package de.comhix.database.dao.test

import com.google.common.collect.Lists
import de.comhix.database.dao.Query
import de.comhix.database.objects.QueryResult

/**
 * @author Benjamin Beeker
 */
open class MockQuery<Type> @JvmOverloads constructor(private val data: Collection<Type> = Lists.newArrayList()) : Query<Type>() {
    constructor(vararg data: Type) : this(Lists.newArrayList(*data))

    override fun <AllowedValueType> with(field: String, operation: Operation<AllowedValueType>, value: AllowedValueType): Query<Type> {
        return this
    }

    override fun limit(limit: Int): Query<Type> {
        return this
    }

    override fun skip(skip: Int): Query<Type> {
        return this
    }

    override fun order(field: String): Query<Type> {
        return this
    }

    override suspend fun query(): QueryResult<Type> {
        return QueryResult(Lists.newArrayList(data))
    }

    override suspend fun find(): Type? {
        return if (data.isEmpty()) null else data.iterator().next()
    }
}