package de.comhix.database.dao

import de.comhix.database.objects.HasUserId
import kotlin.reflect.KClass

/**
 * @author Benjamin Beeker
 */
open class TypeUserDao<Type : HasUserId>(private val userDao: UserDao, private val typeClass: Class<Type>) : SimpleTypeDao<Type> {
    constructor(userDao: UserDao,typeClass: KClass<Type>):this(userDao,typeClass.java)

    override suspend fun get(id: String): Type? {
        return userDao.get(id, typeClass)
    }

    override suspend fun save(instance: Type): Type {
        return userDao.save(instance)
    }

    override suspend fun delete(id: String) {
        return userDao.delete(id, typeClass)
    }

    override suspend fun query(): Query<Type> {
        return userDao.query(typeClass)
    }
}