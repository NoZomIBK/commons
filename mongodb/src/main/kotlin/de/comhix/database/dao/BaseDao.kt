package de.comhix.database.dao

import de.comhix.database.objects.DatabaseObject
import org.mongodb.morphia.Datastore
import javax.inject.Inject

/**
 * @author Benjamin Beeker
 */
open class BaseDao @Inject constructor(private val datastore: Datastore) : SimpleDao<DatabaseObject> {
    override suspend fun <Type : DatabaseObject> get(id: String, typeClass: Class<Type>): Type? {
        return datastore.get(typeClass, id)
    }

    override suspend fun <Type : DatabaseObject> save(instance: Type): Type {
        val key = datastore.save(instance)
        return datastore.getByKey(instance::class.java as Class<Type>, key)
    }

    override suspend fun <Type : DatabaseObject> delete(id: String, typeClass: Class<Type>) {
        datastore.delete(typeClass, id)
    }

    override suspend fun <Type : DatabaseObject> query(typeClass: Class<Type>): Query<Type> {
        return Query(typeClass, datastore)
    }
}