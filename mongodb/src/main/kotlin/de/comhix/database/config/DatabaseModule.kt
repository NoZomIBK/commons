package de.comhix.database.config

import com.google.common.collect.Lists
import com.google.inject.AbstractModule
import com.mongodb.MongoClient
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import org.mongodb.morphia.Datastore
import org.mongodb.morphia.Morphia

/**
 * @author Benjamin Beeker
 */
class DatabaseModule @JvmOverloads constructor(private val host: String,
                                               private val port: Int,
                                               private val dbName: String,
                                               private val user: String,
                                               private val password: String,
                                               private val authDatabase: String = DEFAULT_AUTH_DB) : AbstractModule() {
    override fun configure() {
        val credential = MongoCredential.createCredential(user, authDatabase, password.toCharArray())
        val mongo = MongoClient(ServerAddress(host, port), Lists.newArrayList(credential))
        val datastore = Morphia().createDatastore(mongo, dbName)
        bind(Datastore::class.java).toInstance(datastore)
    }

    companion object {
        const val DEFAULT_AUTH_DB = "admin"
    }
}
