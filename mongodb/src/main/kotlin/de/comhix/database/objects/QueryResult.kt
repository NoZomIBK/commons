package de.comhix.database.objects

import com.google.common.collect.ForwardingList

/**
 * @author Benjamin Beeker
 */
class QueryResult<Type> @JvmOverloads constructor(private val objects: List<Type>, val count: Long = objects.size.toLong()) : ForwardingList<Type>() {
    /**
     * The total number of matching objects found in the database for this query
     *
     * @return matching objects count
     * @since 1.4.7
     */
    override fun delegate(): List<Type> {
        return objects
    }
}