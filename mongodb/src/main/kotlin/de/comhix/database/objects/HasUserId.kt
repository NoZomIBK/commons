package de.comhix.database.objects

/**
 * @author Benjamin Beeker
 */
abstract class HasUserId : DatabaseObject() {
    open var userId: String? = null
}