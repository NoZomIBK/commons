package de.comhix.database.objects

import org.bson.types.ObjectId
import org.mongodb.morphia.annotations.Id
import org.mongodb.morphia.annotations.Version

/**
 * @author Benjamin Beeker
 */
abstract class DatabaseObject {
    @Id
    open   var id: String = ObjectId().toString()

    @Version
    open  var version: Long = 0
}