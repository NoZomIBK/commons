package de.comhix.database

import com.github.fakemongo.Fongo
import com.google.inject.AbstractModule
import org.mongodb.morphia.Datastore
import org.mongodb.morphia.Morphia

/**
 * @author Benjamin Beeker
 */
class TestDatabaseModule : AbstractModule() {
    override fun configure() {
        val fongo = Fongo("mongo server 1")
        val mongo = fongo.mongo
        val datastore = Morphia().createDatastore(mongo, "test-db")
        this.bind(Datastore::class.java).toInstance(datastore)
    }
}