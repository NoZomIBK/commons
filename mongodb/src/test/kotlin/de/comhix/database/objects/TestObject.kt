package de.comhix.database.objects

/**
 * @author Benjamin Beeker
 */
class TestObject : HasUserId() {
    var authToken: String? = null
}