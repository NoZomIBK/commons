package de.comhix.database.dao

import de.comhix.commons.logging.info
import de.comhix.database.TestDatabaseModule
import de.comhix.database.dao.Query.Operation.Companion.EQ
import de.comhix.database.objects.TestObject
import org.amshove.kluent.`should be null`
import org.amshove.kluent.`should contain`
import org.amshove.kluent.`should not be null`
import org.amshove.kluent.`should not contain`
import org.mongodb.morphia.Datastore
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Guice
import org.testng.annotations.Test
import javax.inject.Inject

/**
 * @author Benjamin Beeker
 */
@Guice(modules = [TestDatabaseModule::class])
open class QueryTest {
    @Inject
    lateinit var datastore: Datastore

    @BeforeMethod
    fun cleanup() {
        datastore.db.dropDatabase()
    }

    @Test(timeOut = 30000)
    suspend fun testSimpleQuery() {
        info { "testSimpleQuery" }

        // Given
        val user1 = TestObject()
        user1.authToken = "bla"
        val user2 = TestObject()
        user2.authToken = "authToken"
        datastore.save(user1)
        datastore.save(user2)

        // When
        val query = queryProvider()
        query.with("authToken", EQ, "authToken")
        val queryList: List<TestObject> = query.query()

        // Then
        queryList `should contain` user2
        queryList `should not contain` user1
    }

    @Test(timeOut = 30000)
    suspend fun testLimitQuery() {
        info { "testLimitQuery" }

        // Given
        val user1 = TestObject()
        val user2 = TestObject()
        val user3 = TestObject()
        datastore.save(user1)
        datastore.save(user2)
        datastore.save(user3)

        // When
        val query = queryProvider()
        query.limit(2)
        val queryList: List<TestObject> = query.query()

        // Then
        queryList `should contain` user1
        queryList `should contain` user2
    }

    @Test
    suspend fun testFind_existing() {
        info { "testFind_existing" }

        // Given
        val user1 = TestObject()
        datastore.save(user1)

        // When
        val query = queryProvider()
        val found = query.find()

        // Then
        found.`should not be null`()
    }

    @Test
    suspend fun testFind_notExisting() {
        info { "testFind_notExisting" }

        // When
        val query = queryProvider()
        val notFindable = query.find()

        // Then
        notFindable.`should be null`()
    }

    protected open suspend fun queryProvider(): Query<TestObject> {
        return Query(TestObject::class.java, datastore)
    }
}
