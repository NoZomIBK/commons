package de.comhix.database.dao

import de.comhix.commons.logging.info
import de.comhix.database.TestDatabaseModule
import de.comhix.database.objects.TestObject
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be null`
import org.amshove.kluent.`should not be null`
import org.slf4j.LoggerFactory
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Guice
import org.testng.annotations.Test

/**
 * @author Benjamin Beeker
 */
@Guice(modules = [TestDatabaseModule::class])
class TypeDaoTest : QueryTest() {

    private lateinit var typeDao: SyncTypeDaoWrapper<TestObject>

    @BeforeMethod
    fun setup() {
        typeDao = SyncTypeDaoWrapper(TypeDao(BaseDao(datastore), TestObject::class.java))
    }

    @Test(timeOut = 30000)
     fun testSave() {
        info { "testSave_own" }

        // Given
        val user1 = TestObject()
        user1.authToken = "bla"

        // When
        typeDao.save(user1)

        // Then
        val saved = datastore.get(TestObject::class.java, user1.id)
        saved.`should not be null`()
        saved.authToken `should be equal to` user1.authToken
    }

    @Test(timeOut = 30000)
     fun testGet() {
        info { "testGet" }

        // Given
        val user1 = TestObject()
        user1.authToken = "bla"
        typeDao.save(user1)

        // When
        val saved = typeDao.get(user1.id)

        // Then
        saved.`should not be null`()
        saved.authToken `should be equal to` user1.authToken
    }

    @Test(timeOut = 30000)
     fun testDelete() {
        info { "testDelete" }

        // Given
        val user1 = TestObject()
        user1.authToken = "bla"
        typeDao.save(user1)

        // When
        typeDao.delete(user1.id)

        // Then
        val notExisting = typeDao.get(user1.id)
        notExisting.`should be null`()
    }
}