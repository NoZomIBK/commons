package de.comhix.database.dao

import de.comhix.commons.logging.info
import de.comhix.database.TestDatabaseModule
import de.comhix.database.objects.HasUserId
import de.comhix.database.objects.TestObject
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be null`
import org.amshove.kluent.`should not be null`
import org.bson.types.ObjectId
import org.mongodb.morphia.Datastore
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Guice
import org.testng.annotations.Test
import javax.inject.Inject

/**
 * @author Benjamin Beeker
 */
@Guice(modules = [TestDatabaseModule::class])
class UserDaoTest {
    @Inject
    private lateinit var datastore: Datastore

    private lateinit var userA: String
    private lateinit var userB: String
    private lateinit var userADao: SyncDaoWrapper<HasUserId>
    private lateinit var userBDao: SyncDaoWrapper<HasUserId>

    @BeforeMethod
    fun setup() {
        userA = ObjectId().toString()
        userB = ObjectId().toString()
        val baseDao = BaseDao(datastore)
        userADao = SyncDaoWrapper(UserDao(baseDao, userA))
        userBDao = SyncDaoWrapper(UserDao(baseDao, userB))
    }

    @Test(timeOut = 30000)
    fun testSave_own() {
        info { "testSave_own" }

        // Given
        val `object` = TestObject()

        // When
        val saved = userADao.save(`object`)

        // Then
        saved.userId `should be equal to` userA
    }

    @Test
    fun testSave_doNotOverwriteDifferentUser() {
        info { "testSave_doNotOverwriteDifferentUser" }

        // Given
        val `object` = TestObject()
        val object2 = userBDao.save(`object`)

        // When
        val saved = userADao.save(object2)

        // Then
        saved.userId `should be equal to` userA
        val bObject = userBDao.get(`object`.id, TestObject::class.java)
        bObject.`should not be null`()
        bObject.userId `should be equal to` userB
    }

    @Test(timeOut = 30000)
    fun testSave_differentUser() {
        info { "testSave_differentUser" }

        // Given
        val `object` = TestObject()
        `object`.userId = userB

        // When
        val saved = userADao.save(`object`)

        // Then
        saved.userId `should be equal to` userA
    }

    @Test(timeOut = 30000)
    fun testGet_own() {
        info { "testGet_own" }

        // Given
        val `object` = TestObject()
        userADao.save(`object`)

        // When
        val saved = userADao.get(`object`.id, TestObject::class.java)

        // Then
        saved.`should not be null`()
        saved.userId `should be equal to` userA
    }

    @Test(timeOut = 30000)
    fun testGet_notExisting() {
        info { "testGet_notExisting" }

        // When
        val isEmpty = userADao.get("something", TestObject::class.java)

        // Then
        isEmpty.`should be null`()
    }

    @Test(timeOut = 30000)
    fun testGet_differentUser() {
        info { "testGet_differentUser" }

        // Given
        val `object` = TestObject()
        userADao.save(`object`)

        // When
        val isEmpty = userBDao.get(`object`.id, TestObject::class.java)

        // Then
        isEmpty.`should be null`()
    }

    @Test(timeOut = 30000)
    fun testDelete_own() {
        info { "testDelete_own" }

        // Given
        val `object` = TestObject()
        userADao.save(`object`)

        // When
        userADao.delete(`object`.id, TestObject::class.java)

        // Then
        val isEmpty = userADao.get(`object`.id, TestObject::class.java)
        isEmpty.`should be null`()
    }

    @Test(timeOut = 30000)
    fun testDelete_notExisting() {
        info { "testDelete_notExisting" }

        // When
        userADao.delete("something", TestObject::class.java)

        // Then
        val isEmpty = userADao.get("something", TestObject::class.java)
        isEmpty.`should be null`()
    }

    @Test(timeOut = 30000)
    fun testDelete_differentUser() {
        info { "testDelete_differentUser" }

        // Given
        val `object` = TestObject()
        userADao.save(`object`)

        // When
        userBDao.delete(`object`.id, TestObject::class.java)

        // Then
        val isEmpty = userADao.get(`object`.id, TestObject::class.java)
        isEmpty.`should not be null`()
    }
}