#!/bin/sh -x

. .ci/repo-base.sh

if [[ ! -d "public" ]]
then
    mkdir public
fi

tar -czf ${REPO_FILE} -C ${BUILD_REPO} ./
cp -r ${BUILD_REPO}/* public/
cp ${REPO_FILE} public/