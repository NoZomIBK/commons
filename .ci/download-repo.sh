#!/bin/sh -x

. .ci/repo-base.sh

curl -f https://${CI_PROJECT_NAMESPACE}.gitlab.io/${CI_PROJECT_NAME}/${REPO_FILE} -O || echo "no old repo downloaded"

if [[ -f ${REPO_FILE} ]]
then
    if [[ ! -d "${BUILD_REPO}" ]]
    then
        mkdir -p ${BUILD_REPO}
    fi

    tar -xzf ${REPO_FILE} -C ${BUILD_REPO}
fi