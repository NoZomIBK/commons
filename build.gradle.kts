import de.undercouch.gradle.tasks.download.Download
import io.codearte.gradle.nexus.NexusStagingExtension

plugins {
    base
    java
    kotlin("jvm") version "1.4.10" //apply false
    id("jacoco")
    id("maven-publish")
    id("signing")
    id("org.jetbrains.dokka") version "0.10.0"
    id("de.undercouch.download") version "4.0.4"
    id("io.codearte.nexus-staging") version "0.22.0" apply false
}

data class Version(val major: Int, val minor: Int, val build: Int?) {
    override fun toString(): String {
        return "$major.$minor.${build ?: "0-SNAPSHOT"}"
    }

    val isSnapshot = build == null
}

allprojects {
    group = "de.comhix.commons"
    version = Version(3, 5, System.getenv("CI_PIPELINE_IID")?.toIntOrNull())

    repositories {
        mavenCentral()
        jcenter()
    }
}

val sonatypeUsername: String? by project
val sonatypePassword: String? by project

subprojects {
    apply(plugin = "java")
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "maven-publish")
    apply(plugin = "signing")
    apply(plugin = "jacoco")
    apply(plugin = "org.jetbrains.dokka")

    dependencies {
        implementation(kotlin("stdlib"))
        testImplementation(project(":logging"))
        testImplementation(kotlin("test"))
        testImplementation(kotlin("test-testng"))
        testImplementation("org.amshove.kluent:kluent:[1.61,)")
    }

    jacoco.toolVersion = "0.8.5"

    tasks.compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    tasks.compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }

    tasks.jar {
        manifest {
            attributes["Implementation-Version"] = project.version
        }
        destinationDirectory.set(file("$buildDir/output"))
        archiveFileName.set("${project.name}-${project.version}.${archiveExtension.get()}")
    }

    tasks.jacocoTestReport {
        reports {
            xml.isEnabled = false
            csv.isEnabled = false
            html.destination = file("${buildDir}/reports/jacocoHtml")
        }
    }

    tasks.withType<Test> {
        useTestNG()
    }

    tasks.check {
        dependsOn(tasks.jacocoTestReport)
    }

    tasks.dokka {
        outputFormat = "html"
        outputDirectory = "$buildDir/javadoc"
    }

    val dokkaJar by tasks.creating(Jar::class) {
        group = JavaBasePlugin.DOCUMENTATION_GROUP
        description = "Assembles Kotlin docs with Dokka"
        archiveClassifier.set("javadoc")
        from(tasks.dokka)
        destinationDirectory.set(file("$buildDir/output"))
    }

    val sourcesJar by tasks.creating(Jar::class) {
        archiveClassifier.set("sources")
        from(sourceSets.main.get().allSource)
        destinationDirectory.set(file("$buildDir/output"))
    }

    publishing {
        publications {
            create<MavenPublication>("default") {
                from(components["java"])
                artifact(dokkaJar)
                artifact(sourcesJar)
                pom {
                    name.set("ComHix common parent")
                    description.set("ComHix common basic stuff")
                    url.set("https://gitlab.com/NoZomIBK")
                    licenses {
                        license {
                            name.set("CC-BY-NC-SA")
                            url.set("https://creativecommons.org/licenses/by-nc-sa/3.0/de/")
                        }
                    }
                    developers {
                        developer {
                            id.set("nozomibk")
                            name.set("Benjamin Beeker")
                            email.set("Benjamin.Beeker@comhix.de")
                        }
                    }
                    scm {
                        connection.set("scm:git@gitlab.com:NoZomIBK/commons.git")
                        developerConnection.set("scm:git@gitlab.com:NoZomIBK/commons.git")
                        url.set("https://gitlab.com/NoZomIBK/commons")
                    }
                }
            }
        }
        repositories {
            maven {
                credentials {
                    username = sonatypeUsername
                    password = sonatypePassword
                }
                val releasesRepoUrl = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2/")
                val snapshotsRepoUrl = uri("$buildDir/repos/snapshots")
                url = if ((version as Version).isSnapshot) snapshotsRepoUrl else releasesRepoUrl
            }
        }
    }

    signing {
        isRequired = !(project.version as Version).isSnapshot
        sign(publishing.publications["default"])
    }
}

val versionBadge by tasks.creating(Download::class) {
    val version = version.toString().replace("-", ".")
    src("https://img.shields.io/badge/Version-$version-blue")
    dest("$buildDir/version.svg")
}

apply(plugin = "io.codearte.nexus-staging")

configure<NexusStagingExtension> {
    username = sonatypeUsername
    password = sonatypePassword
    packageGroup = "de.comhix"
    serverUrl = "https://oss.sonatype.org/service/local"
}