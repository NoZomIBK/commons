dependencies {
    implementation("com.google.guava:guava:[11,)")
    implementation("com.google.code.gson:gson:[2.8,)")
}

publishing {
    publications {
        getByName<MavenPublication>("default") {
            pom {
                name.set("ComHix kotlin utils")
                description.set("Kotlin extensions and stuff")
            }
        }
    }
}