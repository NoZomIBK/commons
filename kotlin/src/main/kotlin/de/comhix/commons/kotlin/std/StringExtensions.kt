@file:Suppress("unused")

package de.comhix.commons.kotlin.std

/**
 * @author Benjamin Beeker
 */
fun String.split(splitter: String): Pair<String, String> = this.splitNullable(splitter).let { Pair(it.first!!, it.second!!) }

fun String?.splitNullable(splitter: String): Pair<String?, String?> {
    return this?.split(splitter, limit = 2)?.let {
        Pair(it[0], if (it.size > 1) it[1] else null)
    } ?: Pair(null, null)
}