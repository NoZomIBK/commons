@file:Suppress("unused")

package de.comhix.commons.kotlin.guava

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache

/**
 * @author Benjamin Beeker
 */
fun <Key, Value> CacheBuilder<Key, Value>.build(loader: (key: Key) -> Value?): LoadingCache<Key, Value> {
    return this.build(CacheLoader.from { key: Key? -> loader(key!!) })
}