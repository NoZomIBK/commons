@file:Suppress("unused")

package de.comhix.commons.kotlin.gson

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import java.io.Reader
import kotlin.reflect.KClass

/**
 * @author Benjamin Beeker
 */
inline fun <reified Type> Gson.fromJson(json: String): Type {
    val type = object : TypeToken<Type>() {}.type
    return this.fromJson(json, type)
}

inline fun <reified Type> Gson.fromJson(json: Reader): Type {
    val type = object : TypeToken<Type>() {}.type
    return this.fromJson(json, type)
}

inline fun <reified Type> Gson.fromJson(json: JsonReader): Type {
    val type = object : TypeToken<Type>() {}.type
    return this.fromJson(json, type)
}

inline fun <reified Type> Gson.fromJson(json: JsonElement): Type {
    val type = object : TypeToken<Type>() {}.type
    return this.fromJson(json, type)
}

inline fun <reified Type : Any> KClass<Type>.fromJson(json: String): Type {
    return Gson().fromJson(json, this.java)
}

inline fun <reified Type : Any> KClass<Type>.fromJson(json: Reader): Type {
    return Gson().fromJson(json, this.java)
}

inline fun <reified Type : Any> KClass<Type>.fromJson(json: JsonReader): Type {
    return Gson().fromJson(json, this.java)
}

inline fun <reified Type : Any> KClass<Type>.fromJson(json: JsonElement): Type {
    return Gson().fromJson(json, this.java)
}

fun Any?.toJson(): String = Gson().toJson(this)