package de.comhix.commons.kotlin.std

import de.comhix.commons.logging.info
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be null`
import org.amshove.kluent.`should not be null`
import org.amshove.kluent.`should throw`
import org.testng.annotations.Test

/**
 * @author Benjamin Beeker
 */
class StringExtensionsTest {
    @Test
    fun `splitNullable empty string`() {
        info { "split empty string" }

        // when
        val (left, right) = "".splitNullable(" ")

        // then
        left `should be equal to` ""
        right.`should be null`()
    }

    @Test
    fun `splitNullable single string`() {
        info { "split single string" }

        // when
        val (left, right) = "foo".splitNullable(" ")

        // then
        left `should be equal to` "foo"
        right.`should be null`()
    }

    @Test
    fun `splitNullable split null`() {
        info { "splitNullable split null" }

        // when
        val pair = null.splitNullable(" ")

        // then
        pair.`should not be null`()
        pair.first.`should be null`()
        pair.second.`should be null`()
    }

    @Test
    fun `split empty string`() {
        info { "split empty string" }

        // when
        val callable = { "".split(" ") }

        // then
        callable `should throw` NullPointerException::class
    }

    @Test
    fun `split single string`() {
        info { "split single string" }

        // when
        val callable = { "foo".split(" ") }

        // then
        callable `should throw` NullPointerException::class
    }

    @Test
    fun `split tailing space`() {
        info { "split tailing space" }

        // when
        val (left, right) = "foo ".split(" ")

        // then
        left `should be equal to` "foo"
        right `should be equal to` ""
    }

    @Test
    fun `split pair`() {
        info { "split pair" }

        // when
        val (left, right) = "foo bar".split(" ")

        // then
        left `should be equal to` "foo"
        right `should be equal to` "bar"
    }

    @Test
    fun `split multiple parts`() {
        info { "split multiple parts" }

        // when
        val (left, right) = "foo bar baz".split(" ")

        // then
        left `should be equal to` "foo"
        right `should be equal to` "bar baz"
    }
}