package de.comhix.commons.kotlin.gson

import com.google.gson.Gson
import de.comhix.commons.logging.info
import org.amshove.kluent.`should be equal to`
import org.testng.annotations.Test

/**
 * @author Benjamin Beeker
 */
class GsonExtensionsTest {
    @Test
    fun `from json by class`() {
        info { "from json by class" }

        // given
        val json = "{\"data\":\"foo\"}"

        // when
        val parsed = TestData::class.fromJson(json)

        // then
        parsed.data `should be equal to` "foo"
    }

    @Test
    fun `from json with gson`() {
        info { "from json with gson" }

        // given
        val gson = Gson()
        val json = "{\"data\":\"foo\"}"

        // when
        val parsed: TestData = gson.fromJson(json)

        // then
        parsed.data `should be equal to` "foo"
    }
}

private data class TestData(val data: String)
